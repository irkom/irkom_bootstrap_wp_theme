# Wordpress Theme with Bootstrap#

## Intro ##
### What you will learn ###
[x] Front to Back wordpress theme development
[x] Customized Page layouts - Main posts,  pages, archives, search results, etc.
[x] How to implement Twitter Bootstrap
[x] Theme Customizer API
[x] Some Extra PHP


### What You Should Know ###
[x] HTML / CSS
[x] Basic wordpress
[x] Basic PHP
[x] Programming Fundamental

### What to build ###
[x] Custom menu using nav walker
[x] Wordpress post loop
[x] Comment Functionality
[x] Custom header and footer
[x] Sidebar Widgets
[x] Post Thumbs & Meta Data
[x] Custom Template
[x] Post Format
[x] Archives and Search pages
[x] Custom Front pages
[x] Theme Customizer API

### What you need ###
[x] Theme Stylesheet - https://codex.wordpress.org/Theme_Development
[x] screenshot.png in the root repository
[x] dummy text - http://www.lipsum.com/

### Common Variable ###
[x] language_attributes()
[x] bloginfo('name')
[x] bloginfo('charset')
[x] bloginfo('description')
[x] bloginfo('name')
[x] bloginfo('author')
[x] bloginfo('template_url')
[x] bloginfo('stylesheet_url')
[x] wp_title()
[x] is_front_page()
[x] wp_head()
[x] wp_footer()
[x] get_header()
[x] have_posts()
[x] the_post()
[x] the_date()
[x] the_time()
[x] the_content()
[x] the_excerpt()
[x] get_author_posts_url()
[x] get_the_author_meta('ID')
[x] add_theme_support('post_thumbnails');
[x] has_post_thumbnail()
[x] the_post_thumbnail()
[x] get_template_part('content')
[x] is_single()

### libraries ###
[x] bootstrap navwalker https://github.com/wp-bootstrap/wp-bootstrap-navwalker

### Referrences ###
[x] bootsnipp https://bootsnipp.com
[x] bootstrap navwalker https://github.com/wp-bootstrap/wp-bootstrap-navwalker
[x] wordpress date and time https://codex.wordpress.org/Formatting_Date_and_Time
[x] wodpress list comments https://codex.wordpress.org/Function_Reference/wp_list_comments
[x] wordpress custom comment form https://codex.wordpress.org/Function_Reference/comment_form
